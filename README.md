# Scientific Python SISEO #2

Scientific Python tutorial @ SISEO. Session 2.

Needs:
- [x] : data processing (csv, xls)
- [x] : stats (pandas, ...)
- [ ] : graph theory
- [x] : fit data (scipy, matplotlib)
- [ ] : MD, simulations
- [ ] : Work with meshes for CAD
- [ ] : plots (publication ready)
- [x] : image processing (scales, basic stuff)
- [ ] : ?
